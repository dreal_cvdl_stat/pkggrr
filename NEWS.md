# pkgGRR 0.0.2

* Fix bug dans index.html dans tab_pool (inversion UD36 et UD28)

# pkgGRR 0.0.1

* Modification de importAgent pour exploiter le fichier agent exportable par quiconque a partir de Melanie2
* rajout du calcul du taux de saturation du parc des véhicules dans le rmd

# pkgGRR 0.0.0.9000

* Deploiement du package a partir du fichier extrait sur mesure par l UI
* Added a `NEWS.md` file to track changes to the package.
