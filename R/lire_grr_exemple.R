#' lire_grr_exemple
#' Permet de charger le jeu de donnees exemple GRR
#'
#' @return un dataframe
#' @importFrom data.table fread
#' @importFrom dplyr filter %>%
#' @importFrom stringr str_detect
#' @importFrom tricky set_standard_names
#' @importFrom stringi stri_enc_toascii
#' @export
#'
#' @examples
#'
#' lire_grr_exemple()
#'

lire_grr_exemple <- function(){
  file_grr <- pkggrr::pkggrr_file("data-examples/20204T_GRR.csv")
    #lecture du fichier
  GRR<-data.table::fread(file_grr,head=T,encoding="Latin-1")%>%
    #standardisation des noms de colonnes
  tricky::set_standard_names()

    # passage en ascii pour que la CI passe
  names(GRR)<-stringi::stri_enc_toascii(names(GRR))

  GRR<-GRR %>% dplyr::filter(!stringr::str_detect(.data$ressource_, "VISIO|Pieuvre|Carte|Visio|Salle"))

  return(GRR)

}
