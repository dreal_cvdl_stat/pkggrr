#' creationperiodedemijournee
#' Permet la creation d une table avec chaque periode/demi journee entre une date de debut et une date de fin
#' @param ddeb date ou vecteur de date (la fonction prendra alors la date min) (avec ou sans hms)
#' @param dfin date ou vecteur de date (la fonction prendra alors la date max) (avec ou sans hms)
#'
#' @return dataframe
#' @importFrom dplyr filter mutate select %>%
#' @importFrom lubridate make_datetime year month day %--% wday dmy_hms date %m+% days
#' @export
#'
#' @examples
#' debut=lubridate::dmy_hms("01/01/2021 08:00:00")
#' fin=lubridate::dmy_hms("01/07/2021 15:00:00")
#' creationperiodedemijournee(debut,fin)


creationperiodedemijournee<-function(ddeb,dfin){
deb=min(lubridate::date(ddeb),na.rm=T)
fin=max(lubridate::date(dfin),na.rm=T)

#jours ouvres
dd<-data.frame(vec=deb %m+% lubridate::days(x = 0:as.integer(fin-deb)))%>%
  dplyr::filter(lubridate::wday(.data$vec) %in% c(2:6)) %>%
  dplyr::mutate(a=lubridate::make_datetime(year=lubridate::year(.data$vec),
                                           month=lubridate::month(.data$vec),
                                           day=lubridate::day(.data$vec),hour=0,min=0,sec=0),
                b=lubridate::make_datetime(year=lubridate::year(.data$vec),
                                           month=lubridate::month(.data$vec),
                                           day=lubridate::day(.data$vec),hour=11,min=59,sec=59),
         matinee=.data$a%--%.data$b,
         a=lubridate::make_datetime(year=lubridate::year(.data$vec),
                                    month=lubridate::month(.data$vec),
                                    day=lubridate::day(.data$vec),hour=12,min=0,sec=0),
         b=lubridate::make_datetime(year=lubridate::year(.data$vec),
                                    month=lubridate::month(.data$vec),
                                    day=lubridate::day(.data$vec),hour=23,min=59,sec=59),
         apresmidi=.data$a%--%.data$b) %>%
  dplyr::select(-c(.data$a,.data$b))
}
