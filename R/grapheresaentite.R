#' grapheresaentite
#' Sortie graphique du nombre de reservation par entite (par matinee ou apres midi).
#' @param mydata la table agrege du nombre de reservation par entite; 2 colonnes: name et matinfreq/apresmidifreq
#' @param demijournee matin_nb ou apresmidi_nb
#' @param date_deb date de debut de periode au format date
#' @param date_fin date de fin de periode au format date
#'
#' @importFrom dplyr case_when
#' @importFrom ggiraph geom_bar_interactive ggiraph
#' @importFrom ggplot2 ggplot aes scale_y_continuous theme element_text labs
#' @importFrom gouvdown theme_gouv
#' @importFrom rlang enquo quo_name
#' @importFrom scales percent
#' @importFrom stats reorder
#'
#' @return ggplot
#' @export
#'
#' @examples
#' \dontrun{
#' library(dplyr)
#' grr<-lire_grr_exemple()
#' agent<-lire_agent_exemple()
#' grr_agent<-jointureGRRAgent(fic_grr=grr,fic_agent=agent)
#' t<-traitementGRRduree(grr_agent)
#' t<-calculstatresa(t)
#'
#' tz<-t[[2]]
#'
#' grapheresaentite(tz,matinfreq,
#'                 date_deb=min(pull(t[[1]],vec)),
#'                 date_fin=max(pull(t[[1]],vec)))
#'                 }

grapheresaentite<-function(mydata,demijournee=.data$matinfreq,date_deb,date_fin){

  var<-rlang::enquo(demijournee)

  # creation du titre selon si c est le matin ou l apres midi demande
  titre<-dplyr::case_when(rlang::quo_name(var)=="matinfreq"~"Matin",
                          rlang::quo_name(var)=="apresmidifreq"~"Apr\u00e8s-midi",
                          TRUE~"INCONNU")

  # preparation ud formatage des dates
  # deb<-format(date_deb,"%d/%m/%y")
  # fin<-format(date_fin,"%d/%m/%y")


gg<-ggplot2::ggplot(data=mydata)+
  ggiraph::geom_bar_interactive(ggplot2::aes(x=stats::reorder(.data$name,-!!var),y=!!var,fill=.data$name),stat="identity",
                                data_id = row.names(mydata),
                                tooltip = paste(pull(mydata, .data$name),
                                                scales::percent(pull(mydata,!!var)/100),sep="\n"))+
  ggplot2::scale_y_continuous(breaks=seq(0,round(max(pull(mydata,!!var)),-1),5),expand = c(0,0))+
  gouvdown::theme_gouv()+
  ggplot2::theme(legend.position="none",
        plot.title= ggplot2::element_text(size=14 ),
        plot.subtitle= ggplot2::element_text(size=13 ),
        plot.caption= ggplot2::element_text(size=11 ),
        legend.text= ggplot2::element_text(size=13 ),
        axis.text.x = ggplot2::element_text(size=10,angle=45,hjust = 1 ))+
ggplot2::labs(title=paste0("R\u00e9partition du nombre de r\u00e9servations par entit\u00e9 \n",titre),
              subtitle=paste("Entre le",date_deb,"et le",date_fin),
              y="Pourcentage",
              x="",
              caption="Source : Extraction GRR \nR\u00e9alisation: \u00a9DREAL Centre-Val de Loire")
ggiraph::ggiraph(code = print(gg))

}
