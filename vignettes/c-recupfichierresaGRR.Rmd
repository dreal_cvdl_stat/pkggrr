---
title: "c- Recuperation du fichier des reservations sur GRR"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{c- Recuperation du fichier des reservations sur GRR}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

# Récupération du fichier des réservations GRR

- Se rendre sur GRR en étant authentifié

- Dans la partie en haut à droite, **cliquez** sur Recherche-Rapports-Stats (ATTENTION: des droits spécifiques doivent être accordés par l'UI pour pouvoir accèder à cette rubrique)

![](img/GRR1.PNG)

- **Complétez le formulaire comme suit:**
  
  - 1) Entrer la période de réservations souhaitée
  - 2) Choisir l'option "Valide au moins une des conditions suivantes"
  - 3) Completer les 3 premieres conditions en selectionnant le "Domaine" et en spécifiant "buffon", "coulomb" et "ud"
  - 4) Choisir l'option "Fichier CSV des réservations"
  
  ![](img/GRR2.PNG)
