
<!-- README.md is generated from README.Rmd. Please edit that file -->

# pkggrr

Le site web de présentation du package est :

  - pour la version validée (master) :
    <https://dreal_cvdl_stat.gitlab.io/pkggrr/index.html>  
  - pour la version en développement (dev) :
    <https://dreal_cvdl_stat.gitlab.io/pkggrr/dev/index.html>

<!-- badges: start -->

<!-- badges: end -->

Le but de {pkgGRR} est de faciliter la production de rapports qualité
sur les réservations de véhicules.

Il contient un template bookdown paramétrable et les fonctions
nécessaires à la création des illustrations.

Vous pouvez installer pkgGRR depuis gitlab avec :

``` r
remotes::install_gitlab("dreal_cvdl_stat/pkggrr")
```

## Utilisation

  - Créez un nouveau projet Rstudio, dans un nouveau répertoire et
    sélectionner `Rapport GRR DREAL Centre-Val de Loire` comme type de
    projet ;

  - Sélectionner vos fichiers d’entrée;

  - Complétez les champs auteurs et date dans index.Rmd ;

  - Lancez la compilation du Rmarkdown ;

  - Intégrez vos commentaires et analyses ;-)
